# Freeze_layers

The goal is to develop a python script to generate neat QE and eQE input files for WO3 slabs [001, 100, 111] The atomic coordinates and cell parameters are obtained by ASE (surface module) in the Jupyter environment.