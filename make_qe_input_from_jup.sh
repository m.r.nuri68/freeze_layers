#!/bin/bash

qe_input_jup=$1
layers_to_relax=$2
qe_input_final=$3
#vasp_input=$4

O_per_layer=6
W_per_layer=2
atoms_per_layer=$((O_per_layer + W_per_layer))

n_of_atoms=`grep nat ${qe_input_jup} | awk -F "," '{print $1}' | awk '{print $3}'`
n_of_layers=`expr "scale=0;${n_of_atoms}/${atoms_per_layer}" | bc -l`
n_of_Os=$((O_per_layer * n_of_layers))
n_of_Ws=$((W_per_layer * n_of_layers))

st=`grep -n "ATOMIC_POS" ${qe_input_jup} | grep -Po '^[^:]+'` #the line number of the atomic position of the first atom
strt=`expr "scale=0;${st}+1" | bc -l`
end=$((${strt}+(${n_of_atoms})-1))

cell_params=(`grep CELL_PARAMETERS ${qe_input_jup} -A 3 | tail -3`)
O_coordinates=`sed -n "${strt},${end}p" ${qe_input_jup} | sort -k 1 | head -n ${n_of_Os} | sort -rnk 4`
W_coordinates=`sed -n "${strt},${end}p" ${qe_input_jup} | sort -k 1 | tail -n ${n_of_Ws} | sort -rnk 4`

cat > ${qe_input_final} << EOF
&control
    calculation = 'scf'
    restart_mode = 'from_scratch'
    prefix = 'WO3'
    outdir = 'WO3.wfx'
    tstress = .true.
    tprnfor = .true.
    verbosity = 'high'
    pseudo_dir = '/home/mn657/Pseudo_potentials/GBRV/'
    nstep = 100
 /
&system
!    input_dft = 'PBE'
    ibrav = 0
    nat = ${n_of_atoms}, ntyp = 2
    ecutwfc = 50
    ecutrho = 500
    occupations = 'smearing'
    smearing = 'gaussian'
    degauss = 0.001
!    nosym = .true.
!    nbnd = 136
 /
&electrons
    diagonalization = 'david'
    mixing_mode = 'plain'
    mixing_beta = 0.2
    conv_thr =  1.0d-8
    electron_maxstep = 200
 /
&ions
    ion_dynamics = 'bfgs'
/
ATOMIC_SPECIES
W   183.84  w_pbe_v1.2.uspp.F.UPF
O   15.999  o_pbe_v1.2.uspp.F.UPF

CELL_PARAMETERS angstrom
${cell_params[@]:0:3}
${cell_params[@]:3:3}
${cell_params[@]:6:3}

ATOMIC_POSITIONS angstrom
`echo "${O_coordinates}" | awk '{print $1,$2,$3,$4,0,0,0}' | awk -v n_r_O=$((layers_to_relax * O_per_layer)) '{if(NR>=1 && NR<=n_r_O){print $1,$2,$3,$4}else{print $0}}'`
`echo "${W_coordinates}" | awk '{print $1,$2,$3,$4,0,0,0}' | awk -v n_r_W=$((layers_to_relax * W_per_layer)) '{if(NR>=1 && NR<=n_r_W){print $1,$2,$3,$4}else{print $0}}'`

K_POINTS (automatic)
4 4 1 0 0 0
EOF

#cat > ${vasp_input} << EOF
#vasp
#1.0
#${cell_params[@]:0:3}
#${cell_params[@]:3:3}
#${cell_params[@]:6:3}
#O W
#${n_of_Os} ${n_of_Ws}
#C
#`echo "${O_coordinates}" | awk '{print $2, $3, $4}'`
#`echo "${W_coordinates}" | awk '{print $2, $3, $4}'`
#EOF
